public class Sheep {
    public static final int DELTA_Y = 3;
    private static final int FENCE_BOTTOM_X = 70;
    private static final int FENCE_SPACE = 4;
    public static final int INACTIVE = 0;
    public static final int RUN = 1;
    public static final int JUMP_UP = 2;
    public static final int JUMP_DOWN = 3;
    private static final String SHEEP_IMAGE01 = "sheep00.png";
    private static final String SHEEP_IMAGE02 = "sheep01.png";
    private final PImage sheepImages[] = {loadImage(SHEEP_IMAGE01),loadImage(SHEEP_IMAGE02)};
    private final int SHEEP_IMAGE_WIDTH = sheepImages[0].width;
    private final int SHEEP_IMAGE_HEIGHT = sheepImages[0].height;
    private int x;
    private int y;
    private int jump_x;
    private int canvasWidth;
    private int canvasHeight;
    private boolean stretch;
    private int state = INACTIVE;
    private Shepherd shepherd;
    
    public static final int DELTA_X = 5;

    private int calcJumpX(int initY) {
        initY -= (canvasHeight - SKY_HEIGHT);
        return (FENCE_BOTTOM_X - DELTA_Y * initY / FENCE_SPACE);
    }

    public Sheep(int canvasWidth, int canvasHeight) {
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public PImage getImage() {
        stretch = !stretch;
        if (state == JUMP_UP || state == JUMP_DOWN || stretch) {
            return sheepImages[0];
        } else {
            return sheepImages[1];
        }
    }

    public void update() {
        int currentState = state;
        x -= DELTA_X;
        if (x > jump_x) {
            state = RUN;
        } else if (x > jump_x - SHEEP_IMAGE_WIDTH) {
            state = JUMP_UP;
            y -= DELTA_Y;
        } else if (x > jump_x - 2 * SHEEP_IMAGE_WIDTH) {
            state = JUMP_DOWN;
            y += DELTA_Y;
        } else if (x > -1 * SHEEP_IMAGE_WIDTH) {
            state = RUN;
        } else {
            state = INACTIVE;
        }
        if (currentState != state) {
            shepherd.update(this, state);
        }
    }

    public void resurrection() {
        x = canvasWidth + SHEEP_IMAGE_WIDTH;
        y = SKY_HEIGHT + (int)random(canvasHeight - SKY_HEIGHT - SHEEP_IMAGE_HEIGHT);
        jump_x = calcJumpX(y);
    }
    
    public void addObserver(Shepherd shepherd) {
      this.shepherd = shepherd;
    }
}
