#How to run WoolMark for Processing
1. Clone

    //Please specify woolmark_processing at the time of clone. "-" is invalid character in Processing project.

    $ clone git clone git@bitbucket.org:woolmark/woolmark-processing.git woolmark_processing

2. Launch Processing

3. Open woolmark_processing/woolmark_processing.pde

4. Run