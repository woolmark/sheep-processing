public class Shepherd extends Thread {
    private Sheep[] EMPTY_SHEEP_ARRAY = new Sheep[0];
    private ArrayList<Sheep> activeSheep = new ArrayList<Sheep>();
    private ArrayList<Sheep> inactiveSheep = new ArrayList<Sheep>();
    private int sheepNumber = 0;
    private static final String SHEEP_IMAGE01 = "sheep00.png";
    public static final int INITIAL_SHEEP_NUM = 1;
    public static final int SPEED = 60;

    public Shepherd(int width, int height) {
        PImage sheepImage = loadImage(SHEEP_IMAGE01);
        int sheepMax = (width + 2 * sheepImage.width + (Sheep.DELTA_X - 1)) / Sheep.DELTA_X + INITIAL_SHEEP_NUM;
        for (int i = 0; i < sheepMax; i++) {
            inactiveSheep.add(new Sheep(width, height));
        }
        Thread t = new Thread(this);
        t.start();
    }

    public void update(Sheep s, int state) {
        switch (state) {
        case Sheep.JUMP_DOWN:
            sheepNumber++;
            break;
        case Sheep.INACTIVE:
            activeSheep.remove(s);
            inactiveSheep.add(s);
            if (activeSheep.size() == 0) {
                addSheep();
            }
            break;
        default:
            break;
        }
    }

    public int getSheepNumber() {
        return sheepNumber;
    }

    public void addSheep() {
        Sheep s = inactiveSheep.remove(0);
        s.resurrection();
        activeSheep.add(s);
        s.addObserver(this);
    }

    public Sheep[] getSheep() {
        return activeSheep.toArray(EMPTY_SHEEP_ARRAY);
    }

    public void run() {
        while(true) {
          delay(SPEED);
          Sheep[] sheep = activeSheep.toArray(EMPTY_SHEEP_ARRAY);
          for (Sheep s : sheep) {
              s.update();
          }
        }
    }
}
