private final String FENCE_IMAGE = "fence.png";
private final String MESSAGE = "羊が%d匹";
private final color SKY_COLOR = color(150, 150, 255);
private final color GROUND_COLOR = color(100, 255, 100);
private final color MESSAGE_COLOR = color(0, 0, 0);
private static final int SKY_HEIGHT = 50;
private static final int WIDTH = 120;
private static final int HEIGHT = 120;
private static final int MESSAGE_X = 5;
private static final int MESSAGE_Y = 15;
private PImage fence;
private int fence_x;
private int fence_y;
private Shepherd shepherd;
private boolean isMousePressed = false;


void setup() {
  size(WIDTH, HEIGHT);
  fence = loadImage(FENCE_IMAGE);
  fence_x = (WIDTH - fence.width)/2;
  fence_y = (WIDTH - fence.height);
  smooth();
  noStroke();
  shepherd = new Shepherd(WIDTH, HEIGHT);
  for (int i = 0; i < Shepherd.INITIAL_SHEEP_NUM; i++) {
    shepherd.addSheep();
  }
  thread("checkMouse");
}

void draw() {
  colorMode(RGB, 256);
  background(GROUND_COLOR);
  fill(SKY_COLOR);
  rect(0, 0, WIDTH, SKY_HEIGHT);
  image(fence, fence_x, fence_y, fence.width, fence.height);
  
  for (Sheep s : shepherd.getSheep()) {
     if(s != null) {
       image(s.getImage(), s.getX(), s.getY(), s.getImage().width, s.getImage().height);
     }
  }

  fill(MESSAGE_COLOR);
  text(String.format(MESSAGE, shepherd.getSheepNumber()), MESSAGE_X, MESSAGE_Y);
}


void checkMouse() {
  while(true) {
    delay(Shepherd.SPEED);
    if (isMousePressed) {
      shepherd.addSheep();
    }
  }
}

void mousePressed(){
  isMousePressed = true;
}

void mouseReleased(){
  isMousePressed = false;
}



